pipeline {
    agent { label 'packer' }
    environment {
        KERNEL_RT_VERSION = "3.10.0-693.21.1.rt56.639.el7.x86_64"
    }

    stages {
        stage('Validate') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'packer validate centos-rt.json'
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    sh 'packer build centos-rt.json'
                }
            }
        }
        stage('Upload') {
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "build/esss-centos-7-rt.ova",
                          "target": "swi-pkg/virtualbox/${KERNEL_RT_VERSION}/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
