Packer template to build a CentOS 7 virtualbox with RT kernel
=============================================================

This Packer_ template creates a CentOS 7 VirtualBox with RT kernel.
It's used to build kernel modules.

Usage
-----

The archive is built automatically and uploaded to artifactory by Jenkins
(you can check the Jenkinsfile).

To build the box locally, run::

    $ packer build centos-rt.json

This will create the "build/esss-centos-7-rt.ova" box.


.. _Packer: https://www.packer.io
